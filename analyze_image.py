#!/usr/bin/env python3

import argparse
import os

from PIL import Image

import resnet50
import id2label
import numpy as np


def main():
    args = get_args()

    net = resnet50.ResNet50(os.path.abspath(args.weights))

    img = get_image(args.image, net.image_size)
    print('image: {}'.format(args.image))

    features = net.get_features([img])
    print('features: {}'.format(features.shape))

    classes = net.get_classes([img])
    print('classes: {}'.format(get_top_cls_names(classes)))


def get_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='Process image by ImageNet conv net, print feature shape and top classes'
    )

    parser.add_argument('-w', '--weights', default=None, type=str, metavar='WEIGHTS',
                        help='Path to tensorflow checkpoint with weights.')

    parser.add_argument('-i', '--image', default=None, type=str, metavar='FILENAME',
                        help='Path to image file.')

    args = parser.parse_args()
    return args


def get_image(filename, image_size):
    img = Image.open(filename)
    img = img.resize((image_size, image_size))
    img = np.array(img)
    assert img.shape == (image_size, image_size, 3)
    return img


def get_top_cls_names(classes):
    return [id2label.id2label[index] for index in classes[0].argsort()[-5:]][::-1]


if __name__ == '__main__':
    main()
