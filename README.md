# Minimal ConvNet Wrappers

- minimal wrappers for extracting features for images

## Installation
```
$ pip install --upgrade -r requirements.txt
```

## Download and extract pretrained weights

- from [tensorflow github](https://github.com/tensorflow/models/tree/master/slim)
```
$ curl -LO 'http://download.tensorflow.org/models/resnet_v1_50_2016_08_28.tar.gz'
$ tar xvf resnet_v1_50_2016_08_28.tar.gz
```

- should create tensorflow checkopoint `resnet_v1_50.ckpt`

## Run on sample image

![Image of dog with a ball](dog.jpg)

```
$ ./analyze_image.py -w resnet_v1_50.ckpt -i dog.jpg
image: dog.jpg
features: (1, 2048)
classes: ['soccer ball', 'volleyball', 'parachute, chute', 'umbrella', 'golden retriever']
```

![Image of a small brown fluffy rabbit](rabbit.jpg)
```
$ ./analyze_image.py -w resnet_v1_50.ckpt -i rabbit.jpg
image: rabbit.jpg
features: (1, 2048)
classes: ['hare', 'wood rabbit, cottontail, cottontail rabbit', 'Angora, Angora rabbit', 'bath towel', 'teddy, teddy bear']
```