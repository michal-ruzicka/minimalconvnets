#!/usr/bin/env python3

import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim.python.slim.nets import resnet_v1


class ResNet50:
    def __init__(self, weights):
        self.image_size = resnet_v1.resnet_v1.default_image_size

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self._graph = tf.Graph()
        self._sess = tf.Session(graph=self._graph, config=config)

        with self._graph.as_default():
            self._tfinput = tf.placeholder(dtype=tf.float32, shape=[None, self.image_size, self.image_size, 3])

            with slim.arg_scope(resnet_v1.resnet_arg_scope(is_training=False)):
                self._classes, endpoints = resnet_v1.resnet_v1_50(self._tfinput, num_classes=1000)

            self._features = tf.get_default_graph().get_tensor_by_name('resnet_v1_50/pool5:0')

            init_fn = slim.assign_from_checkpoint_fn(
                weights,
                slim.get_model_variables('resnet_v1_50')
            )

            init_fn(self._sess)

    def get_classes(self, images):
        with self._graph.as_default():
            classes = self._sess.run([self._classes], feed_dict={self._tfinput: images})
            return classes[0].reshape(-1, 1000)

    def get_features(self, images):
        with self._graph.as_default():
            features = self._sess.run([self._features], feed_dict={self._tfinput: images})
            return features[0].reshape(-1, 2048)

    def __del__(self):
        self._sess.close()
